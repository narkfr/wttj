# Welcome to the jungle hiring Test

Hiring test for Welcome to the jungle. The React setup is based on my personal [React bootstrap](https://framagit.org/narkfr/react-boot)

## Installation

```
git clone git@framagit.org:narkfr/wttj.git
cd wttj
yarn install
yarn start
```

## Configuration

### Columns and rows

You can choose the columns and rows you want to display, by adding a query in the url, like this: `/?columns=2&rows=2`.
If you don't set a query, default parameters will be 3 columns and 2 rows.

With Great Power Comes Great Responsibility : if you set `/?columns=0&rows=2`, you'll see... 0 columns. What you want is what you get.

### Autoplay

The `<Slider>` component have an `autoplay` props, who take a bool as parameter. No surprise, `true` will launch autoplay, `false` won't. It's set to `false` in the repository.

## Redux

Redux is used to import the blocks. It's a very minimalist setup, since the project don't need more.
It just import a json with an array of blocks, like a back-end API could send it.
If you want to see more complex redux work I did, you can look my others recent projects in [Framagit](https://framagit.org/narkfr)

## i18n

The content of the blocks are in french, I don't think that's the front-end job to translate back-end content.
Otherwise, i18n is not an option today so it's a part of my minimal react setup.
