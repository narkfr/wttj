// https://gist.github.com/andjosh/6764939
const scrollTo = (element, to, duration) => {
  const elementToScroll = element;
  const start = elementToScroll.scrollLeft;

  const change = to - start;

  let currentTime = 0;

  const increment = 20;

  const animateScroll = () => {
    currentTime += increment;
    const val = Math.easeInOutQuad(currentTime, start, change, duration);
    elementToScroll.scrollLeft = val;
    if (currentTime < duration) {
      setTimeout(animateScroll, increment);
    }
  };
  animateScroll();
};

Math.easeInOutQuad = (t, b, c, d) => {
  t /= d / 2;
  if (t < 1) return (c / 2) * t * t + b;
  t--;
  return (-c / 2) * (t * (t - 2) - 1) + b;
};

export default scrollTo;
