const sliceArrayInChunk = (array, chunkSize) =>
  array
    .map(
      (item, index) =>
        index % chunkSize === 0 ? array.slice(index, index + chunkSize) : null
    )
    .filter(item => item);

export default sliceArrayInChunk;
