import blocks from "../assets/blocks.json";

export const IMPORT_BLOCKS = "IMPORT_BLOCKS";
export const importBlocks = () =>
  function action(dispatch) {
    dispatch({
      type: IMPORT_BLOCKS,
      payload: blocks
    });
  };
