import PropTypes from "prop-types";
import React from "react";

import "./RoundButton.scss";

const RoundButton = ({ onClick, size, color, children }) => (
  <button
    type="button"
    className={`button button--${size} button--${color}`}
    onClick={() => onClick()}
  >
    <div className="button__children">{children}</div>
  </button>
);

RoundButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  size: PropTypes.oneOf(["small", "medium", "big"]).isRequired,
  color: PropTypes.oneOf(["grey", "green"]).isRequired,
  children: PropTypes.string.isRequired
};

export default RoundButton;
