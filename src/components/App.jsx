/* global window */
import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";

import { importBlocks } from "../actions/blocks";
import sliceArrayInChunk from "../libs/sliceArrayInChunk";

import Slider from "./Slider/Slider";
import SliderFooter from "./Slider/SliderFooter";

import "./App.scss";

class App extends Component {
  // Define parameters and fallback for columns and rows
  columnsFallback = "3";

  rowsFallback = "2";

  urlParams = new URLSearchParams(window.location.search);

  static propTypes = {
    importBlocks: PropTypes.func.isRequired,
    blocks: PropTypes.arrayOf(PropTypes.object).isRequired
  };

  constructor(props) {
    super(props);
    this.wrapper = React.createRef();
  }

  componentWillMount = () => {
    this.defineColumns();
    this.defineRows();
  };

  defineColumns = () => {
    this.setState({
      columns:
        window.innerWidth <= 480
          ? "1"
          : this.urlParams.get("columns") || this.columnsFallback
    });
  };

  defineRows = () =>
    this.setState({
      rows: this.urlParams.get("rows") || this.rowsFallback
    });

  defineWrapperWidth = () =>
    this.setState({ wrapperWidth: this.wrapper.current.clientWidth });

  handleResize = () => {
    this.defineColumns();
    this.defineWrapperWidth();
  };

  componentDidMount = () => {
    const { importBlocks } = this.props; // eslint-disable-line
    importBlocks();
    window.addEventListener("resize", this.handleResize);
    this.defineWrapperWidth();
  };

  componentWillUnmount = () => {
    window.removeEventListener("resize", this.handleResize);
    // Initial wrapperWidth definition
    this.defineWrapperWidth();
  };

  render() {
    const { columns, rows, wrapperWidth } = this.state;
    const { blocks } = this.props;
    // blocks array is sliced in slides
    // a slide is the visible part of the wrapper, defined by the number of
    // columns and rows
    const slides = sliceArrayInChunk(blocks, columns * rows);
    return (
      <main className="wttj" ref={this.wrapper}>
        {wrapperWidth && (
          <Slider
            wrapperWidth={wrapperWidth}
            autoplay={false}
            columns={columns}
            rows={rows}
            slides={slides}
          />
        )}
        <SliderFooter />
      </main>
    );
  }
}

export default connect(
  // mapStateToProps
  state => state.blocks,
  // mapDispatchToProps
  { importBlocks }
)(App);
