import PropTypes from "prop-types";
import React, { Component, Fragment } from "react";
import { translate } from "react-i18next";

import RoundButton from "../Common/RoundButton";
import Slide from "./Slide";

import scrollTo from "../../libs/scrollTo";

import "./Slider.scss";
import wttjLogoWhite from "../../assets/wttj-square--white.svg";

// TODO:
// - autoplay
class Slider extends Component {
  static propTypes = {
    autoplay: PropTypes.bool.isRequired,
    slides: PropTypes.arrayOf(PropTypes.array).isRequired,
    columns: PropTypes.string.isRequired,
    rows: PropTypes.string.isRequired,
    wrapperWidth: PropTypes.number.isRequired,
    t: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.slider = React.createRef();
    this.state = {
      touchstartX: 0,
      touchendX: 0,
      autoplayInterval: null
    };
  }

  componentDidMount = () => {
    const { autoplay } = this.props;
    this.defineScrollStep();
    const autoplayInterval = autoplay && setInterval(this.scrollRight, 2000);
    autoplayInterval && this.setState({ autoplayInterval }); // eslint-disable-line
    this.slider.current.addEventListener("touchstart", event =>
      this.handleTouchStart(event)
    );
    this.slider.current.addEventListener("touchend", event =>
      this.handleTouchEnd(event)
    );
  };

  componentDidUpdate = prevProps => {
    // Check if the width of the wrapper has been updated to recalculate the scrollStep
    const { wrapperWidth } = this.props;
    prevProps.wrapperWidth !== wrapperWidth && this.defineScrollStep(); // eslint-disable-line
  };

  componentWillUnmount = () => {
    const { autoplay } = this.props;
    const { autoplayInterval } = this.state;
    autoplay && clearInterval(autoplayInterval); // eslint-disable-line
    this.slider.current.removeEventListener("touchstart", event =>
      this.handleTouchStart(event)
    );
    this.slider.current.removeEventListener("touchend", event =>
      this.handleTouchEnd(event)
    );
  };

  handleTouchStart = event =>
    this.setState({
      touchstartX: event.changedTouches[0].screenX
    });

  handleTouchEnd = event => {
    this.setState({
      touchendX: event.changedTouches[0].screenX
    });
    this.handleGesture();
  };

  handleGesture = () => {
    const { touchstartX, touchendX } = this.state;
    if (touchendX < touchstartX) {
      this.scrollRight();
    }

    if (touchendX > touchstartX) {
      this.scrollLeft();
    }
  };

  defineScrollStep = () => {
    const { columns, wrapperWidth } = this.props;
    // A scrollStep is the width of a column.
    // It's calculated in pixel
    this.setState({ scrollStep: wrapperWidth / columns });
  };

  scrollLeft = () => {
    const { scrollStep } = this.state;
    scrollTo(
      this.slider.current,
      this.slider.current.scrollLeft - scrollStep,
      1000
    );
  };

  scrollRight = () => {
    const { scrollStep } = this.state;
    scrollTo(
      this.slider.current,
      this.slider.current.scrollLeft + scrollStep,
      1000
    );
  };

  render() {
    const { slides, columns, rows, t } = this.props;
    return (
      <Fragment>
        <div className="slider__header">
          <img
            className="slider__header__logo"
            src={wttjLogoWhite}
            alt="Welcome to the jungle logo"
          />
          <h1 className="slider__header__title">{t("wttj")}</h1>
          <div className="slider__header__buttons">
            <RoundButton
              size="small"
              color="grey"
              onClick={() => this.scrollLeft()}
            >
              &lsaquo;
            </RoundButton>
            <RoundButton
              size="small"
              color="grey"
              onClick={() => this.scrollRight()}
            >
              &rsaquo;
            </RoundButton>
          </div>
        </div>
        <div className="slider" ref={this.slider}>
          {slides.map(slide => (
            <Slide
              key={slides.indexOf(slide)}
              slide={slide}
              columns={columns}
              rows={rows}
            />
          ))}
        </div>
      </Fragment>
    );
  }
}

export default translate("translations")(Slider);
