import PropTypes from "prop-types";
import React from "react";

import Image from "../Blocks/Image";
import Quote from "../Blocks/Quote";
import Video from "../Blocks/Video";

const Cell = ({ type, content }) => {
  const blocks = {
    video: Video,
    quote: Quote,
    image: Image
  };
  const Block = blocks[`${type}`];
  return <Block content={content} />;
};

Cell.propTypes = {
  type: PropTypes.string.isRequired,
  content: PropTypes.object.isRequired // eslint-disable-line
};

export default Cell;
