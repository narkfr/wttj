import PropTypes from "prop-types";
import React from "react";
import { translate } from "react-i18next";

import RoundButton from "../Common/RoundButton";
import { setLSItem } from "../../libs/localStorage";

import wttjLogo from "../../assets/wttj-square.svg";

import "./SliderFooter.scss";

const SliderFooter = ({ i18n }) => {
  const changeLanguage = lng => {
    i18n.changeLanguage(lng);
    setLSItem("language", lng);
  };
  return (
    <div className="slider__footer">
      <img
        className="slider__footer__logo"
        src={wttjLogo}
        alt="Welcome to the jungle logo"
      />
      <div className="slider__footer_button">
        <RoundButton
          size="medium"
          color="grey"
          onClick={() => changeLanguage("fr")}
        >
          fr
        </RoundButton>
        <RoundButton
          size="medium"
          color="grey"
          onClick={() => changeLanguage("en")}
        >
          en
        </RoundButton>
      </div>
    </div>
  );
};

SliderFooter.propTypes = {
  i18n: PropTypes.object.isRequired // eslint-disable-line
};

export default translate("translations")(SliderFooter);
