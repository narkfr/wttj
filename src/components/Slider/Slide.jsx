import PropTypes from "prop-types";
import React from "react";

import sliceArrayInChunk from "../../libs/sliceArrayInChunk";

import Cell from "./Cell";

import "./Slide.scss";

const Slide = ({ slide, columns, rows }) => {
  // Slice the array in chunk of row to get the real number of column
  const colNumber = sliceArrayInChunk(slide, rows).length;
  // Cross Multiplication to determinate the width of the slide to avoid displaying
  // an incomplete slide
  const sliceWidth = columns === colNumber ? 100 : (100 / columns) * colNumber;
  return (
    <div
      className="slider__slide"
      style={{
        minWidth: `${sliceWidth}%`,
        maxWidth: `${sliceWidth}%`,
        gridTemplateColumns: `repeat(${colNumber}, 1fr)`,
        gridTemplateRows: `repeat(${rows}, 1fr)`
      }}
    >
      {slide.map(cell => (
        <Cell
          key={slide.indexOf(cell)}
          content={cell.content}
          type={cell.type}
        />
      ))}
    </div>
  );
};

Slide.propTypes = {
  slide: PropTypes.arrayOf(PropTypes.object).isRequired,
  columns: PropTypes.string.isRequired,
  rows: PropTypes.string.isRequired
};

export default Slide;
