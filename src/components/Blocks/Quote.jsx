import PropTypes from "prop-types";
import React from "react";

import "./Quote.scss";
import quote from "../../assets/quote.png";

const Quote = ({ content }) => (
  <div className="block block__quote">
    <div className="block__overlay" />
    <img src={quote} alt="quote" className="block__quote__quotation-mark" />
    <p className="block__quote__text">{content.quote}</p>
  </div>
);

Quote.propTypes = {
  content: PropTypes.shape({
    quote: PropTypes.string
  }).isRequired
};

export default Quote;
