import PropTypes from "prop-types";
import React, { Component, Fragment } from "react";
import ModalVideo from "react-modal-video";

import RoundButton from "../Common/RoundButton";

import "./Video.scss";
import "react-modal-video/scss/modal-video.scss";

class Video extends Component {
  static propTypes = {
    content: PropTypes.shape({
      title: PropTypes.string,
      subtitle: PropTypes.string,
      thumbnail: PropTypes.string,
      videoId: PropTypes.string
    }).isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    };
  }

  openModal = () => {
    this.setState({ isOpen: true });
  };

  closeModal = () => {
    this.setState({ isOpen: false });
  };

  render() {
    const { content } = this.props;
    const { isOpen } = this.state;
    return (
      <Fragment>
        <ModalVideo
          channel="youtube"
          isOpen={isOpen}
          videoId={content.videoId}
          onClose={() => this.closeModal()}
        />
        <div
          className="block block__video"
          style={{ backgroundImage: `url(${content.thumbnail})` }}
        >
          <div className="block__overlay" />
          <div className="block__video__content">
            <div className="block__video__button">
              <RoundButton size="big" color="green" onClick={() => this.openModal()}>
                &#x25B8;
              </RoundButton>
            </div>
            <div className="block__video__texts">
              <h2>{content.title}</h2>
              <h3>{content.subtitle}</h3>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Video;
