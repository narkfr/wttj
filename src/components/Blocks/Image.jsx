import PropTypes from "prop-types";
import React from "react";

import "./Image.scss";

import glass from "../../assets/glass.svg";

const Image = ({ content }) => (
  <div
    className="block block__image"
    style={{ backgroundImage: `url(${content.imageUrl})` }}
  >
    <div className="block__overlay" />
    <div className="block__image__zoom">
      <img src={glass} alt="Zoom" />
    </div>
  </div>
);

Image.propTypes = {
  content: PropTypes.shape({
    imageUrl: PropTypes.string
  }).isRequired
};

export default Image;
