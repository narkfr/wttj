/* global document */
import React from "react";
import ReactDOM from "react-dom";
import { I18nextProvider } from "react-i18next";
import { Provider } from "react-redux";

import i18n from "./libs/i18n";
import store from "./store";

import App from "./components/App";

ReactDOM.render(
  <I18nextProvider i18n={i18n}>
    <Provider store={store}>
      <div lang={i18n.language}>
        <App />
      </div>
    </Provider>
  </I18nextProvider>,
  document.getElementById("app")
);

// Allow hot reload
module.hot.accept();
