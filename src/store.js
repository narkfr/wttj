import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";

import * as blocks from "./reducers/blocks";

const middleware = [thunk];

const store = createStore(
  combineReducers({
    ...blocks
  }),
  applyMiddleware(...middleware)
);

export default store;
