import { IMPORT_BLOCKS } from "../actions/blocks";

export function blocks( // eslint-disable-line
  state = {
    blocks: []
  },
  { type, payload }
) {
  switch (type) {
    case IMPORT_BLOCKS:
      return {
        ...state,
        blocks: payload
      };

    default:
      return state;
  }
}
